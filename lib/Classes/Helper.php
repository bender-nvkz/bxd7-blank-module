<?php
namespace My\BlankModule;
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Helper
{
    public static function OrderingList()
    {
        return BlankModuleTable::getList(array(
            'order' => array('ID' =>'ASC'),
        ));
    }
}
