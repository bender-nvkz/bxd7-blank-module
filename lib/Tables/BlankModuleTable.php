<?php
namespace My\BlankModule;
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\StringField;
use Bitrix\Main\Entity\TextField;
use Bitrix\Main\Entity\BooleanField;
use Bitrix\Main\Entity\DateField;
use Bitrix\Main\Entity\Validator;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type;

Loc::loadMessages(__FILE__);

class BlankModuleTable extends DataManager
{

    public static function getTableName()
    {
        return 'blankmodulerestbl';
    }

    public static function getMap()
    {
        return array(
            new IntegerField('ID', array(
                'autocomplete' => true,
                'primary' => true,
                'title' => Loc::getMessage('ID'),
            )),
            new StringField('NAME', array(
                'required' => true,
                'title' => Loc::getMessage('NAME'),
                'default_value' => function () {
                    return Loc::getMessage('NAME_DEFAULT_VALUE');
                },
                'validation' => function () {
                    return array(
                        new Validator\Length(5, 255),
                        new Validator\Unique()
                    );
                },
            )),
            new BooleanField('ACTIVE', array(
                'title' => Loc::getMessage('ACTIVE'),
                'default_value' => function () {
                    return false;
                }
            )),
            new DateField('DATE_CREATE', array(
                'title' => Loc::getMessage('DATE_CREATE'),
                'default_value' => function () {
                    return new Type\Date(date('Y-m-d', strtotime('now')), 'Y-m-d');
                }
            )),
            new TextField('DESC', array(
                'title' => Loc::getMessage('DESC'),
                'default_value' => null
            )),
        );
    }
}