<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$aMenu = array(
    array(
        'parent_menu' => 'global_menu_services',
        'icon' => 'menu_fileman',
        'sort' => 400,
        'text' => Loc::getMessage("MAIN_LINK_TEXT"),
        'title' => Loc::getMessage("MAIN_LINK_TITLE"),
        'url' => 'blankmodule.php',
        'items_id' => 'menu_references',
        /*
        'items' => array(
            array(
                'text' => Loc::getMessage("SUB_LINK_TEXT"),
                'url' => 'blankmodule.php?param1=paramval&lang='.LANGUAGE_ID,
                'more_url' => array('projects_index.php?param1=paramval&lang='.LANGUAGE_ID),
                'title' => Loc::getMessage("SUB_LINK_TITLE"),
            ),
        ),
        */
    ),
);

return $aMenu;