<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use My\BlankModule\BlankModuleTable as DBModule;

$ID = intval($ID);

$backPageUri = $APPLICATION->GetCurPageParam("action=list", array("ID", "id", "action", "sessid"));
$ResPageUri = $APPLICATION->GetCurPageParam("action=edit&ID=".$ID, array("ID", "id", "action", "sessid"));

if(check_bitrix_sessid())
{
    if($ID>0) {
        @$result = DBModule::delete($ID);
        if($result->isSuccess())
            LocalRedirect($backPageUri);
        else
            LocalRedirect($ResPageUri);
    }
}

LocalRedirect($backPageUri);