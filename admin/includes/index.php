<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php';

if(!CModule::IncludeModule("my.blankmodule"))
{
    require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php';
    CAdminMessage::showMessage("Module 'my.blankmodule' not install.");
    require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php';
    exit;
}

if(isset($action) && file_exists(__DIR__.'/'.$action.'.php'))
    require_once $action.'.php';
else
    require_once 'list.php';

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php';