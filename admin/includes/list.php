<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use My\BlankModule\BlankModuleTable as DBModule;
use Bitrix\Main\Application;

IncludeModuleLangFile(__FILE__);
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/interface/admin_lib.php");

$resMap = DBModule::getMap();
/*
echo '<pre>';
print_r($resMap[0]->getName());
echo '</pre>';
*/

if($action_button == 'delete' && check_bitrix_sessid()) {
    if(!empty($ID) && is_array($ID)) {
        foreach($ID as $eid) {
            @$result = DBModule::delete($eid);
        }
    } elseif ($action_target == 'selected') {
        @$connection = Application::getInstance()->getConnection();
        @$connection->truncateTable(DBModule::getTableName());
    }
}

$dsc_cookie_name = COption::GetOptionString('main', 'cookie_name', 'BITRIX_SM')."_DSC";

if (isset($_REQUEST['mode']) && ($_REQUEST['mode']=='list' || $_REQUEST['mode']=='frame'))
{
    CFile::DisableJSFunction(true);
}

$sTableID = "tbl_iblock_list_".md5(DBModule::getTableName());
$oSort = new CAdminSorting($sTableID, $resMap[0]->getName(), "desc");
$arOrder = (strtoupper($by) === $resMap[0]->getName() ? array($by => $order): array($by => $order, $resMap[0]->getName() => "ASC"));
$lAdmin = new CAdminList($sTableID, $oSort);

$rsData = DBModule::GetList(array('order' => $arOrder));

$arHeader = array();
foreach($resMap as $eDBres) {
    $arHeader[] = array(
        "id" => $eDBres->getName(),
        "content" => $eDBres->getTitle(),
        "sort" => $eDBres->getName(),
        "default" => true,
        "align" => "left",
    );
}
$lAdmin->AddHeaders($arHeader);

$rsData = new CAdminResult($rsData, $sTableID);
$rsData->NavStart();
$lAdmin->NavText(GetMessage("NAV_TEXT"));

while($arRes = $rsData->NavNext(true, "f_"))
{
    $row = $lAdmin->AddRow($f_TYPE.$f_ID, $arRes, $APPLICATION->GetCurPageParam("action=edit&ID=".$f_ID, array("ID", "id", "action", "sessid")));
}

$lAdmin->AddFooter(
    array(
        array("title"=>GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value"=>$rsData->SelectedRowsCount()),
        array("counter"=>true, "title"=>GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value"=>"0"),
    )
);

$arActions = array(
    "delete" => GetMessage("MAIN_ADMIN_LIST_DELETE"),
);
$arParams = array();
$lAdmin->AddGroupActionTable($arActions, $arParams);

$aContext = array();
$aContext[] = array(
    "TEXT" => GetMessage("MAIN_ADMIN_LIST_ADD"),
    "ICON" => "btn_new",
    "LINK" => "?action=edit&ID=0"
);
$lAdmin->AddAdminContextMenu($aContext);

$lAdmin->CheckListMode();

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php';

$lAdmin->DisplayList();