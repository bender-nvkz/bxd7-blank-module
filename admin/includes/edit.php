<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use My\BlankModule\BlankModuleTable as DBModule;

IncludeModuleLangFile(__FILE__);
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/interface/admin_lib.php");

$curPageUri = $APPLICATION->GetCurPageParam("", array("ID", "id"));
$backPageUri = $APPLICATION->GetCurPageParam("action=list", array("ID", "id", "action", "sessid"));
$newPageUri = $APPLICATION->GetCurPageParam("action=edit&ID=0", array("ID", "id", "action", "sessid"));
$delPageUri = $APPLICATION->GetCurPageParam("action=delete&ID=".$ID."&".bitrix_sessid_get(), array("ID", "id", "action", "sessid"));
$ID = intval($ID);
$message = null;

$resMap = DBModule::getMap();

if($REQUEST_METHOD == "POST" && ($save!="" || $apply!="") && check_bitrix_sessid()) {
    $arFields = array();
    foreach($resMap as $eDBres) {
        if ($eDBres->isAutocomplete()) continue;
        switch ($eDBres->getDataType()) {
            case 'date' :
                if(isset(${"str_".$eDBres->getName()}))
                    $arFields[$eDBres->getName()] = new \Bitrix\Main\Type\Date(${"str_".$eDBres->getName()}, 'd.m.Y');
                break;
            case 'boolean' :
                if(isset(${"str_".$eDBres->getName()}) && ${"str_".$eDBres->getName()} > 0)
                    $arFields[$eDBres->getName()] = true;
                else
                    $arFields[$eDBres->getName()] = false;
                break;
            default:
                if(isset(${"str_".$eDBres->getName()}))
                    $arFields[$eDBres->getName()] =${"str_".$eDBres->getName()};
        }
    }
    if($ID == 0)
        @$result = DBModule::add($arFields);
    else
        @$result = DBModule::update($ID,$arFields);
    if($result->isSuccess()) {
        if($apply!="")
            LocalRedirect($APPLICATION->GetCurPageParam("ID=".($ID == 0 ? $result->getId() : $ID), array("ID", "id")));
        elseif($save!="")
            LocalRedirect($backPageUri);
    } else {
        $message = $result->getErrorMessages();
    }
}

if($ID>0){
    $_arRes = DBModule::getRowById($ID);
}

$aTabs = array(
    array("DIV" => "entity_edit1",
        "TAB" => GetMessage("entity_edit1_tab"),
        "TITLE"=>GetMessage("entity_edit1_tab_title")),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);

$APPLICATION->SetTitle(($ID>0? GetMessage("entity_edit_title_edit").$ID : GetMessage("entity_edit_title_add")));

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php';

if(!empty($message))
    CAdminMessage::ShowOldStyleError($message);

$aMenu = array(
    array(
        "TEXT"=>GetMessage("entity_list"),
        "TITLE"=>GetMessage("entity_list_title"),
        "LINK"=>$backPageUri,
        "ICON"=>"btn_list",
    )
);

if($ID>0)
{
    $aMenu[] = array(
        "TEXT"=>GetMessage("entity_add"),
        "TITLE"=>GetMessage("entity_mnu_add"),
        "LINK"=>$newPageUri,
        "ICON"=>"btn_new",
    );
    $aMenu[] = array(
        "TEXT"=>GetMessage("entity_delete"),
        "TITLE"=>GetMessage("entity_mnu_del"),
        "LINK"=>"javascript:if(confirm('".GetMessage("entity_mnu_del_conf")."'))window.location='".$delPageUri."';",
        "ICON"=>"btn_delete",
    );
}
$context = new CAdminContextMenu($aMenu);
$context->Show();
?>

<form method="POST" Action="<?echo $curPageUri?>" ENCTYPE="multipart/form-data" name="post_form">
    <?
    echo bitrix_sessid_post();
    $tabControl->Begin();
    $tabControl->BeginNextTab();
    ?>

    <? foreach($resMap as $eDBres) { if($eDBres->isAutocomplete()) continue;
        $fName = 'str_'.$eDBres->getName();
        if($ID>0)
            switch ($eDBres->getDataType()) {
                case 'date':
                    $fVal = (isset(${"str_".$eDBres->getName()})) ? ${"str_".$eDBres->getName()} : $_arRes[$eDBres->getName()]->__toString();
                    break;
                default:
                    $fVal = (isset(${"str_".$eDBres->getName()})) ? ${"str_".$eDBres->getName()} : $_arRes[$eDBres->getName()];
            }
        else
            $fVal = (isset(${"str_".$eDBres->getName()})) ? ${"str_".$eDBres->getName()} : $eDBres->getDefaultValue();
        ?>
        <tr>
            <td><? if($eDBres->isRequired() || $eDBres->isPrimary()){?><span class="required">*</span><?}?><?echo $eDBres->getTitle();?>:</td>
            <td>
                <? switch ($eDBres->getDataType()) {
                    case 'boolean': ?>
                        <?echo InputType("checkbox", $fName, "1", $fVal)?>
                        <? break; ?>
                    <? case 'date': ?>
                        <?echo CalendarDate($fName, $fVal, "post_form")?>
                        <? break; ?>
                    <? case 'text': ?>
                        <textarea name="<?echo $fName;?>" style="width:100%" rows="10"><?echo $fVal;?></textarea>
                        <? break; ?>
                    <? default: ?>
                        <input type="text"
                               name="<?echo $fName;?>"
                               value="<?echo $fVal;?>"
                            />
                <? } ?>
            </td>
        </tr>

    <? } ?>

    <?
    $tabControl->Buttons(
        array(
            "back_url"=>$backPageUri,
        )
    );
    ?>
    <input type="hidden" name="lang" value="<?=LANG?>">
    <?
    if($ID>0) echo '<input type="hidden" name="ID" value="'.$ID.'">';
    $tabControl->End();
    $tabControl->ShowWarnings("post_form", $message);
    ?>
</form>