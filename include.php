<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Loader;
use Bitrix\Main\EventManager;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

Loader::registerAutoLoadClasses('my.blankmodule', array(
    'My\BlankModule\BlankModuleTable' => 'lib/Tables/BlankModuleTable.php',
    'My\BlankModule\Helper' => 'lib/Classes/Helper.php',
));

EventManager::getInstance()->addEventHandler('main', 'OnBeforeProlog', function(){
    echo 'OnBeforeProlog BlankModule Event';
});