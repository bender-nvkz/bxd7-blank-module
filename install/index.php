<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use My\BlankModule\BlankModuleTable;

Loc::loadMessages(__FILE__);

if (class_exists('my_blankmodule')) {
    return;
}

class my_blankmodule extends CModule
{
    /** @var string */
    public $MODULE_ID;

    /** @var string */
    public $MODULE_VERSION;

    /** @var string */
    public $MODULE_VERSION_DATE;

    /** @var string */
    public $MODULE_NAME;

    /** @var string */
    public $MODULE_DESCRIPTION;

    /** @var string */
    public $MODULE_GROUP_RIGHTS;

    /** @var string */
    public $PARTNER_NAME;

    /** @var string */
    public $PARTNER_URI;

    public function __construct()
    {
        $this->MODULE_ID = 'my.blankmodule';
        $this->MODULE_VERSION = '1.0';
        $this->MODULE_VERSION_DATE = '2015-06-01 17:08:48';
        $this->MODULE_NAME = Loc::getMessage('MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('MODULE_DESCRIPTION');
        $this->MODULE_GROUP_RIGHTS = 'N';
        $this->PARTNER_NAME = "Rekuz";
        $this->PARTNER_URI = "http://pr.rekuz.ru";
    }

    public function doInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
        $this->installDB();
        $this->installFiles();
    }

    public function DoUninstall()
    {
        $this->uninstallDB();
        $this->UnInstallFiles();
        ModuleManager::unregisterModule($this->MODULE_ID);
    }

    public function InstallDB()
    {
        if (Loader::includeModule($this->MODULE_ID)) {
            BlankModuleTable::getEntity()->createDbTable();
        }
    }

    public function installFiles()
    {
        $mod_dir=dirname(dirname(__FILE__)) . '/admin';
        if (Loader::includeModule($this->MODULE_ID)) {
            CopyDirFiles(
                $mod_dir,
                $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin",
                true,
                false,
                false,
                'menu.php'
            );
        }
    }

    public function UnInstallDB()
    {
        if (Loader::includeModule($this->MODULE_ID)) {
            $connection = Application::getInstance()->getConnection();
            $connection->dropTable(BlankModuleTable::getTableName());
        }
    }

    public function UnInstallFiles()
    {
        $mod_dir=dirname(dirname(__FILE__)) . '/admin';
        if (Loader::includeModule($this->MODULE_ID)) {
            DeleteDirFiles(
                $mod_dir,
                $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin",
                array("menu.php")
            );
        }
    }
}
