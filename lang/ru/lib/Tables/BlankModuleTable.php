<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

$MESS['ID'] = 'ИД';
$MESS['NAME'] = 'Имя';
$MESS['NAME_DEFAULT_VALUE'] = 'Новое имя';
$MESS['ACTIVE'] = 'Активность';
$MESS['DATE_CREATE'] = 'Дата создания';
$MESS['DESC'] = 'Описание';