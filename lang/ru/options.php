<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

$MESS['REFERENCES_MY_VAR'] = 'Моя опция';
$MESS['REFERENCES_MY_BOOL'] = 'Моя опция2';
$MESS['VAR_CTRL_TAB_SET'] = 'Опции';
$MESS['VAR_CTRL_TAB_TITLE_SET'] = 'Установка значения опции';
$MESS['MORE_TAB_SET'] = 'Другое';
$MESS['MORE_TAB_TITLE_SET'] = 'Дополнительная вкладка опций';
$MESS['REFERENCES_OPTIONS_RESTORED'] = "Восстановлены настройки по умолчанию";
$MESS['REFERENCES_OPTIONS_SAVED'] = "Настройки сохранены";
$MESS['REFERENCES_INVALID_VALUE'] = "Введено неверное значение";
$MESS['MAIN_SAVE'] = 'Применить';
$MESS['MAIN_OPT_SAVE_TITLE'] = 'Применить изменения';
$MESS['MAIN_HINT_RESTORE_DEFAULTS'] = 'Установить значения по умолчанию';
$MESS['MAIN_HINT_RESTORE_DEFAULTS_WARNING'] = 'Внимание! Все настройки будут перезаписаны значениями по умолчанию. Продолжить?';
$MESS['MAIN_RESTORE_DEFAULTS'] = 'По умолчанию';