<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

$MESS['entity_edit1_tab'] = 'Свойства';
$MESS['entity_edit1_tab_title'] = 'Свойства объекта';
$MESS['entity_edit_title_edit'] = 'Редактирование объекта # ';
$MESS['entity_edit_title_add'] = 'Создание объекта';
$MESS['entity_list'] = 'В список';
$MESS['entity_list_title'] = 'Вернуться к списку объектов';
$MESS['entity_add'] = 'Создать';
$MESS['entity_mnu_add'] = 'Создать новый объект';
$MESS['entity_delete'] = 'Удалить';
$MESS['entity_mnu_del'] = 'Удалить этот объект';
$MESS['entity_mnu_del_conf'] = 'Вы уверены, что хотите удалить этот объект?';
$MESS['entity_edit1_tab'] = 'Свойства';
$MESS['entity_edit1_tab'] = 'Свойства';