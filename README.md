Blank module template for Bitrix D7-core framework.
ORM table, OOP and namespaced (psr-4) based.

Заготовка для создания модуля на платформе Битрикс под ядро D7.
Полагается на ORM технологию, использует ООП, инкапсуляция реализована способом пространств имен.