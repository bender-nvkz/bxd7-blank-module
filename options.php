<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();
defined('ADMIN_MODULE_NAME') or define('ADMIN_MODULE_NAME', 'my.blankmodule');

use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Text\String;

if (!$USER->isAdmin()) {
    $APPLICATION->authForm('Nope');
}

$app = Application::getInstance();
$context = $app->getContext();
$request = $context->getRequest();

Loc::loadMessages(__FILE__);

$tabControl = new CAdminTabControl("tabControl", array(
    array(
        "DIV" => "var_ctrl",
        "TAB" => Loc::getMessage("VAR_CTRL_TAB_SET","Variable"),
        "TITLE" => Loc::getMessage("VAR_CTRL_TAB_TITLE_SET","Variable settings tab"),
    ),
    array(
        "DIV" => "more_tab",
        "TAB" => Loc::getMessage("MORE_TAB_SET","More"),
        "TITLE" => Loc::getMessage("MORE_TAB_TITLE_SET","One more clean tab"),
    ),
));


$arOptionsTab = array(
    array(
        array(
            'NAME' => 'my_var',
            'TYPE' => 'text',
            'VALUE' => Option::get(ADMIN_MODULE_NAME, "my_var"),
            'DESCRIPTION' => Loc::getMessage("REFERENCES_MY_VAR")
        )
    ),
    array(
        array(
            'NAME' => 'my_bool',
            'TYPE' => 'checkbox',
            'VALUE' => Option::get(ADMIN_MODULE_NAME, "my_bool"),
            'DESCRIPTION' => Loc::getMessage("REFERENCES_MY_BOOL")
        )
    )
);

if ((!empty($save) || !empty($restore)) && $request->isPost() && check_bitrix_sessid()) {
    if (!empty($restore)) {
        Option::delete(ADMIN_MODULE_NAME);
        foreach ($arOptionsTab as $i1 => $tabs)
            foreach ($tabs as $i2 => $optCtrl)
                $arOptionsTab[$i1][$i2]['VALUE'] = Option::get(ADMIN_MODULE_NAME, $optCtrl['NAME']);
        CAdminMessage::showMessage(array(
            "MESSAGE" => Loc::getMessage("REFERENCES_OPTIONS_RESTORED"),
            "TYPE" => "OK",
        ));
    } else {
        foreach ($arOptionsTab as $i1 => $tabs)
            foreach ($tabs as $i2 => $optCtrl) {
                if($request->getPost($optCtrl['NAME'])) {
                    Option::set(
                        ADMIN_MODULE_NAME,
                        $optCtrl['NAME'],
                        $request->getPost($optCtrl['NAME'])
                    );
                } else {
                    Option::set(
                        ADMIN_MODULE_NAME,
                        $optCtrl['NAME'],
                        ''
                    );
                }
                $arOptionsTab[$i1][$i2]['VALUE'] = Option::get(ADMIN_MODULE_NAME, $optCtrl['NAME']);
            }
        CAdminMessage::showMessage(array(
            "MESSAGE" => Loc::getMessage("REFERENCES_OPTIONS_SAVED"),
            "TYPE" => "OK",
        ));
    }
}

$tabControl->begin();
?>

<form method="post" action="<?=sprintf('%s?mid=%s&lang=%s', $request->getRequestedPage(), urlencode($mid), LANGUAGE_ID)?>">
    <?
    foreach($arOptionsTab as $tabs) {
        $tabControl->beginNextTab();
        foreach ($tabs as $optCtrl) {
        ?>
            <tr>
                <td width="40%">
                    <label for="<?=$optCtrl['NAME']?>"><?=$optCtrl['DESCRIPTION']?>:</label>
                <td width="60%">
            <?
            switch ($optCtrl['TYPE']) {
                case 'checkbox':
                    ?>
                    <input type="checkbox"
                           name="<?=$optCtrl['NAME']?>"
                           value="Y"
                           <? if(!empty($optCtrl['VALUE'])) echo "checked"; ?>
                        />
                    <?
                    break;
                default:
                    ?>
                    <input type="text"
                        size="50"
                        maxlength="255"
                        name="<?=$optCtrl['NAME']?>"
                        value="<?=String::htmlEncode($optCtrl['VALUE']);?>"
                        />
                    <?
            }
            ?>
                </td>
            </tr>
        <?
        }
    }
    ?>

    <?php
    $tabControl->buttons();
    ?>
    <input type="submit"
        name="save"
        value="<?=Loc::getMessage("MAIN_SAVE") ?>"
        title="<?=Loc::getMessage("MAIN_OPT_SAVE_TITLE") ?>"
        class="adm-btn-save"
        />
    <input type="submit"
        name="restore"
        title="<?=Loc::getMessage("MAIN_HINT_RESTORE_DEFAULTS") ?>"
        onclick="return confirm('<?= AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING")) ?>')"
        value="<?=Loc::getMessage("MAIN_RESTORE_DEFAULTS") ?>"
        />
    <?
    echo bitrix_sessid_post();
    $tabControl->End();
    ?>
</form>